package com.example.android.scorekeeper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA = 0;

    int yellowCardA = 0;

    int redCardA = 0;

    int scoreTeamB = 0;

    int yellowCardB = 0;

    int redCardB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Increase the score for Team A by 1 points.
     */
    public void addOneForTeamA(View v) {
        scoreTeamA = scoreTeamA + 1;
        displayForTeamA(scoreTeamA);
    }

    public void addYellowForTeamA(View v){
        yellowCardA = yellowCardA + 1;
        displayYellowTeamA(yellowCardA);
    }

    public void addRedForTeamA(View v){
        redCardA = redCardA + 1;
        displayRedTeamA(redCardA);
    }

    public void addOneForTeamB(View v){
        scoreTeamB = scoreTeamB + 1;
        displayForTeamB(scoreTeamB);
    }

    public void addYellowForTeamB(View v){
        yellowCardB = yellowCardB + 1;
        displayYellowTeamB(yellowCardB);
    }

    public void addRedForTeamB(View v){
        redCardB = redCardB + 1;
        displayRedTeamB(redCardB);
    }

    public void resetScore(View v){
        scoreTeamA = 0;
        scoreTeamB = 0;
        yellowCardA = 0;
        yellowCardB = 0;
        redCardA = 0;
        redCardB = 0;
        displayForTeamA(scoreTeamA);
        displayForTeamB(scoreTeamB);
        displayYellowTeamA(yellowCardA);
        displayYellowTeamB(yellowCardB);
        displayRedTeamA(redCardA);
        displayRedTeamB(redCardB);
    }

    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayYellowTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_yellowcard);
        scoreView.setText(String.valueOf((score)));
    }

    public void displayRedTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_redcard);
        scoreView.setText(String.valueOf((score)));
    }

    /**
     * Displays the given score for Team B.
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayYellowTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_yellowcard);
        scoreView.setText(String.valueOf((score)));
    }

    public void displayRedTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_redcard);
        scoreView.setText(String.valueOf((score)));
    }
}